package Uzduotys.kitosUzduotys;

/*Programą, kuri skaičiuoja šias operacijas: * / + -.
Vartotojas įveda 2 skaičius ir operacijos ženklą. Pagal įvestą ženklą, atliekama operacija. */

import java.util.*;
import java.lang.*;

public class Kalkuliatorius {
    public static void main(String[] args) {
        double skaicius1 = 1;
        double skaicius2 = 2;
        char operacija = '+';
        char arTesti = 'T';

        System.out.println("Kalkuliatorius");


        while (arTesti == 'T' || arTesti == 't') {

            try {
                Scanner input = new Scanner(System.in); //ivedamas pirmasis skaicius
                System.out.println("Įveskite pirmąjį skaičių");
                skaicius1 = input.nextDouble();
            } catch (Exception e) {
                continue;
            }

            Scanner input2 = new Scanner(System.in); //ivedamas operacijos zenklas
            System.out.println("Įveskite vieną iš šių aritmetinių operacijų: +  -  *  /");
            operacija = input2.next().charAt(0);
            if (operacija !='+' && operacija !='-' && operacija != '*' && operacija != '/') {
                System.out.println("Pasirinkta neleistina aritmetinė operacija");
                continue;
            }

            try {
                Scanner input3 = new Scanner(System.in); //ivedamas antrasis skaicius
                System.out.println("Įveskite antrąjį skaičių");
                skaicius2 = input3.nextDouble();
            } catch (Exception e) {
                continue;
            }

            if (operacija == '+') {
                System.out.println(skaicius1 + " + " + skaicius2 + " = " + (skaicius1 + skaicius2));
            } else if (operacija == '-') {
                System.out.println(skaicius1 + " - " + skaicius2 + " = " + (skaicius1 - skaicius2));
            } else if (operacija == '*') {
                System.out.println(skaicius1 + " * " + skaicius2 + " = " + (skaicius1 * skaicius2));
            } else if (operacija == '/' && skaicius2 == 0) {
                System.out.println("Dalyba iš nulio negalima");
            } else if (operacija == '/') {
                System.out.println(skaicius1 + " / " + skaicius2 + " = " + (skaicius1 / skaicius2));
            }

            System.out.println("Ar norite tęsti skaičiavimą? Jei taip, įveskite T");
            Scanner input4 = new Scanner(System.in); //ivedamas atsakymas
            arTesti = input4.next().charAt(0);
        }
        System.out.println("Skaičiavimas baigtas");
    }

}
