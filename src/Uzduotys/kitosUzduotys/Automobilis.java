package Uzduotys.kitosUzduotys;

public class Automobilis {
public static void main(String[] args) {
    Automobilis auto1 = new Automobilis ("Audi", "raudona", 2013, 4, 50000, false);
    Automobilis auto2 = new Automobilis("BMW", "pilka", 2008, 2, 123000, false);
    Automobilis auto3 = new Automobilis("Citroen", "juoda", 2015, 4, 200000, true);
    Automobilis auto4 = new Automobilis("Dacia", "žalia", 2018, 2, 2000, false);
    Automobilis auto5 = new Automobilis("Ford", "mėlyna", 2000, 4, 50000, true);

    System.out.print("Parduodamas " + auto1.getMarke() + " A4, " + auto1.getPagaminMetai() + " m., " + auto1.getDuruSkaicius() + " durų, spalva - " + auto1.getSpalva() + ", rida - " + auto1.getRida() + " km.");
    auto1.spauzdintiArElektromob();
    System.out.println( " Kaina - 20 tugrikų.");
    auto1.setDuruSkaicius(2);
    System.out.print("Parduodamas " + auto1.getMarke() + " A4, " + auto1.getPagaminMetai() + " m., " + auto1.getDuruSkaicius() + " durų, spalva - " + auto1.getSpalva() + ", rida - " + auto1.getRida() + " km.");
    auto1.spauzdintiArElektromob();
    System.out.println( " Kaina - 19 tugrikų.");
    System.out.print("Parduodamas " + auto2.getMarke() + " X5, " + auto2.getPagaminMetai() + " m., " + auto2.getDuruSkaicius() + " durų, spalva - " + auto2.getSpalva() + ", rida - " + auto2.getRida() + " km.");
    auto1.spauzdintiArElektromob();
    System.out.println( " Kaina - 21 tugrikas.");
    auto2.setElektromobilis(true);
    System.out.print("Parduodamas " + auto2.getMarke() + " X5, " + auto2.getPagaminMetai() + " m., " + auto2.getDuruSkaicius() + " durų, spalva - " + auto2.getSpalva() + ", rida - " + auto2.getRida() + " km.");
    auto2.spauzdintiArElektromob();
    System.out.println( " Kaina - 25 tugrikai.");
    System.out.print("Parduodamas " + auto3.getMarke() + " 6, " + auto3.getPagaminMetai() + " m., " + auto3.getDuruSkaicius() + " durų, spalva - " + auto3.getSpalva() + ", rida - " + auto3.getRida() + " km.");
    auto3.spauzdintiArElektromob();
    System.out.println( " Kaina - 23 tugrikai.");
    auto3.setPagaminMetai(2000);
    System.out.print("Parduodamas " + auto3.getMarke() + " 6, " + auto3.getPagaminMetai() + " m., " + auto3.getDuruSkaicius() + " durų, spalva - " + auto3.getSpalva() + ", rida - " + auto3.getRida() + " km.");
    auto3.spauzdintiArElektromob();
    System.out.println( " Kaina - 21 tugrikas.");
    System.out.print("Parduodamas " + auto4.getMarke() + " Kazkoks, " + auto4.getPagaminMetai() + " m., " + auto4.getDuruSkaicius() + " durų, spalva - " + auto4.getSpalva() + ", rida - " + auto4.getRida() + " km.");
    auto4.spauzdintiArElektromob();
    System.out.println( " Kaina - 12 tugrikų.");
    auto4.setRida(20);
    System.out.print("Parduodamas " + auto1.getMarke() + " Kazkoks, " + auto4.getPagaminMetai() + " m., " + auto4.getDuruSkaicius() + " durų, spalva - " + auto4.getSpalva() + ", rida - " + auto4.getRida() + " km.");
    auto4.spauzdintiArElektromob();
    System.out.println( " Kaina - 13 tugrikų.");
    System.out.print("Parduodamas " + auto5.getMarke() + " Fiesta, " + auto5.getPagaminMetai() + " m., " + auto5.getDuruSkaicius() + " durų, spalva - " + auto5.getSpalva() + ", rida - " + auto5.getRida() + " km.");
    auto5.spauzdintiArElektromob();
    System.out.println( " Kaina - 24 tugrikai.");
    auto5.setSpalva("auksinė");
    System.out.print("Parduodamas " + auto5.getMarke() + " Fiesta, " + auto5.getPagaminMetai() + " m., " + auto5.getDuruSkaicius() + " durų, spalva - " + auto5.getSpalva() + ", rida - " + auto5.getRida() + " km.");
    auto5.spauzdintiArElektromob();
    System.out.println( " Kaina - 29 tugrikai.");
}
    String marke;
    String spalva;
    int pagaminMetai;
    int duruSkaicius;
    int rida;
    boolean elektromobilis;

    public Automobilis(String marke, String spalva,int pagaminMetai, int duruSkaicius, int rida, boolean elektromobilis){
        this.marke = marke;
        this.spalva = spalva;
        this.pagaminMetai = pagaminMetai;
        this.duruSkaicius = duruSkaicius;
        this.rida = rida;
        this.elektromobilis = elektromobilis;
    }

    private void spauzdintiArElektromob(){
        if (elektromobilis == true) {
            System.out.print(", elektromobilis.");
        }
        }


    public String getMarke() {
        return marke;
    }

    public void setMarke(String marke) {
        this.marke = marke;
    }

    public String getSpalva() {
        return spalva;
    }

    public void setSpalva(String spalva) {
        this.spalva = spalva;
    }

    public int getPagaminMetai() {
        return pagaminMetai;
    }

    public void setPagaminMetai(int pagaminMetai) {
        this.pagaminMetai = pagaminMetai;
    }

    public int getDuruSkaicius() {
        return duruSkaicius;
    }

    public void setDuruSkaicius(int duruSkaicius) {
        this.duruSkaicius = duruSkaicius;
    }

    public int getRida() {
        return rida;
    }

    public void setRida(int rida) {
        this.rida = rida;
    }

    public boolean isElektromobilis() {
        return elektromobilis;
    }

    public void setElektromobilis(boolean elektromobilis) {
        this.elektromobilis = elektromobilis;
    }
}





//1. Sukurti klasę Automobilis, panaudojant tinkamiausius duomenų tipus:
//        - markė;
  //      - spalva;
    //    - pagaminimo metai;
      //  - durų skaičius;
        //- rida;
//        - elektramobilis. // (true, false)
  //      2. Sukurti get'erius, set'erius ir konstruktorių.
    //    3. Sukurti 5 klasės objektus (skirtingus).
      //  4. Pakeisti kiekvieno objekto bent po 1 požymį.
        //5. Atspausdinti objektus prieš ir po pakeitimų.