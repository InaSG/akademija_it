package Uzduotys.kitosUzduotys;

import java.util.*;
import java.lang.*;

public class DaugybosLentele {
    public static void main(String[] args) {
        int sk;
        int rezultatas;

        System.out.println("Sandaugos nuo 1 iki 9 skaičiavimas");
        System.out.println("Įveskite sveikąjį skaičių");
        Scanner S = new Scanner(System.in); //Skaiciaus ivedimui
        sk = S.nextInt();
        System.out.println("Skaičiavimas be ciklų");

        System.out.println("Atsakymas");
        rezultatas = sk * 1;
        System.out.println(sk + " * " + 1 + " = " + rezultatas);
        rezultatas = sk * 2;
        System.out.println(sk + " * " + 2 + " = " + rezultatas);
        rezultatas = sk * 3;
        System.out.println(sk + " * " + 3 + " = " + rezultatas);
        rezultatas = sk * 4;
        System.out.println(sk + " * " + 4 + " = " + rezultatas);
        rezultatas = sk * 5;
        System.out.println(sk + " * " + 5 + " = " + rezultatas);
        rezultatas = sk * 6;
        System.out.println(sk + " * " + 6 + " = " + rezultatas);
        rezultatas = sk * 7;
        System.out.println(sk + " * " + 7 + " = " + rezultatas);
        rezultatas = sk * 8;
        System.out.println(sk + " * " + 8 + " = " + rezultatas);
        rezultatas = sk * 9;
        System.out.println(sk + " * " + 9 + " = " + rezultatas);

        System.out.println("Skaičiavimas su for ciklų");

        System.out.println("Atsakymas");
        for (int i = 1; i < 10; i++) {
            System.out.println(sk + " * " + i + " = " + sk * i);
        }

        System.out.println("Skaičiavimas su While ciklų");

        System.out.println("Atsakymas");
        int i = 1;
        while (i < 10) {
            System.out.println(sk + " * " + i + " = " + (sk * i));
            i++;
        }

    }
}
