package Uzduotys.kitosUzduotys;

import java.util.*;
import java.lang.*;

public class VertinimoSist {

    public static void main(String[] args) {
        int balas = -1;

        System.out.println("Vertinimo sistema");

        while (balas != 0) {
            System.out.println("Įveskite moksleivio įvertinimą balais. Jei norite baigti darbą, įveskite 0");
            Scanner S = new Scanner(System.in); //Balo ivedimui
            balas = S.nextInt();
            if (balas == 10) {
                System.out.println("puikios, išskirtinės žinios ir gebėjimai");
            } else if (balas == 9) {
                System.out.println("tvirtos, geros žinios ir gebėjimai");
            } else if (balas == 8) {
                System.out.println("geresnės, nei vidutinės žinios ir gebėjimai");
            } else if (balas == 7) {
                System.out.println("vidutinės žinios ir gebėjimai, yra neesminių klaidų");
            } else if (balas == 6) {
                System.out.println("žinios ir gebėjimai (įgūdžiai) žemesni nei vidutiniai, yra klaidų");
            } else if (balas == 5) {
                System.out.println("žinios ir gebėjimai (įgūdžiai) tenkina minimalius reikalavimus");
            } else if (balas != 0) {
                System.out.println("netenkinami minimalūs reikalavimai");
            }
        }
    }
}