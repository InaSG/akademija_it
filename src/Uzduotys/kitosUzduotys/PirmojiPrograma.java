package Uzduotys.kitosUzduotys;

import java.util.Scanner;

public class PirmojiPrograma {
    public static void main(String[] args) {
        int sk1, sk2; //Pradiniai kintamieji
        int skirtumas; //rezultatas

        System.out.println("Skirtumo skaičiavimas");
        System.out.println("Įveskite pirmą skaičių");
        Scanner S = new Scanner(System.in); //ivedimui
        sk1 = S.nextInt();   //sveikas skaicius
        System.out.println("Įveskite antrą skaičių");
        sk2 = S.nextInt();
        skirtumas = sk1 - sk2;
        System.out.println( sk1 + " - " + sk2 + " = " + skirtumas );

    }
}
