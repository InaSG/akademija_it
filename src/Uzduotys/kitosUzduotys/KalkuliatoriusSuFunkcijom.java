package Uzduotys.kitosUzduotys;
import java.util.*;

public class KalkuliatoriusSuFunkcijom {
    public static void main(String[] args) {
        double skaicius1;
        double skaicius2;
        char veiksmas;
        char arTesti = 't';

        System.out.println("Kalkuliatorius, atliekantis pagrindines aritmetines operacijas");

        while (arTesti == 'T' || arTesti == 't') {

            try {
                Scanner input = new Scanner(System.in);
                prasytiSkaiciaus();
                skaicius1 = input.nextDouble();
            } catch (Exception e) {
                System.out.println(perspetiDelKintamuju());
                continue;
            }
            Scanner input2 = new Scanner(System.in);
            System.out.println("Įveskite vieną iš šių aritmetinių operacijų: +  -  * /");
            veiksmas = input2.next().charAt(0);
            if (veiksmas != '+' && veiksmas != '-' && veiksmas != '*' && veiksmas != '/') {
                System.out.println("Įvedėte neleistiną simbolį");
                continue;
            }
            try {
                Scanner input3 = new Scanner(System.in);
                prasytiSkaiciaus();
                skaicius2 = input3.nextDouble();
            } catch (Exception e) {
                System.out.println(perspetiDelKintamuju());
                continue;
            }

            if (veiksmas == '+'){
                sudeti(skaicius1, skaicius2);
            } else if (veiksmas == '-'){
                System.out.println(skaicius1 + " - " + skaicius2 + " = " + (atimti(skaicius1, skaicius2)));
            } else if (veiksmas == '*'){
                System.out.println(skaicius1 + " * " + skaicius2 + " = " + skaicius1*skaicius2);
            } else if (veiksmas == '/' && skaicius2 == 0) {
                System.out.println("Dalyba iš 0 negalima");
            }else
                System.out.println(skaicius1 + " / " + skaicius2 + " = " + skaicius1/skaicius2);

            Scanner input4 = new Scanner(System.in);
            System.out.println("Jei norite tęsti darbą, spauskite T");
            arTesti = input4.next().charAt(0);
        }

        System.out.println("Darbas baigtas");

    }
    //Negrazinanti reiksmes funkcija su parametrais
    public static void sudeti(double skaicius1, double skaicius2){
        System.out.println(skaicius1 + " + " + skaicius2 + " = " + (skaicius1 + skaicius2));
    }
    //Grazinanti reiksme funkcija su parametrais
    public static double atimti(double skaicius1, double skaicius2){
        double rezultatas;
        rezultatas = skaicius1 - skaicius2;
        return rezultatas;
    }
    //Negrazinanti reiksmes be parametru
    public static void prasytiSkaiciaus(){
       System.out.println("Įveskite skaičių");
    }
    //Grazinanti reiksme be parametru
    public static String perspetiDelKintamuju(){
        return "Įvedėte ne skaičių";
    }

}
