package Uzduotys.kitosUzduotys;

public class Knyga {
    public static void main(String [] args) {

        Knyga kn1 = new Knyga("Miškais ateina ruduo", "Marius Katiliškis", "romanas", 5,
                "Kaunas", 235, true, "879-465-8765-98-3");
        Knyga kn2 = new Knyga("Gydymas Šopenhaueriu", "Irvin Yalom", "romanas", 1,
                "Kaunas", 267,true, "879-8769-456-98-5");
        Knyga kn3 = new Knyga("Psichologiniai sprendimų priėmimo ypatumai", "Auksė Endriulaitienė",
                "monografija", 2, "Kaunas", 235, false,
                "9955-12-158-0");
        Knyga kn4 = new Knyga("Paveikslas", "Vytautė Žilinskaitė", "humoreskos", 1,
                "Kaunas", 262, true, "7865-478-574-63-7");
        Knyga kn5 = new Knyga ("Salos ir jų žmonės", "Algimantas Čekuolis", "publicistika",
                1, "Vilnius", 297, true, "978-609-01-2663-9");
        Knyga kn6 = new Knyga ("Kai Nyčė verkė", "Irvin Yalom", "romanas", 1,
                "Vilnis", 375, true, "978-5-415-01982-3");
        Knyga kn7 = new Knyga ("Lietuviškoji trilogija", "Kazys Bradūnas", "poezija", 3,
                "Vilnius", 139, true, "786-9478-23-345-9");
        Knyga kn8 = new Knyga ("Trise valtimi", "Džeromas K. Džeromas", "apysaka", 2,
                "Vilnius", 194, false, "678-3339-567-2-45");
        Knyga kn9 = new Knyga ("Šachmatų vadovėlis", "Chose Kapablanka", "vadovelis", 3,
                "Vilnius", 168, true, "678-456-678-3-7");
        Knyga kn10 = new Knyga ("Į Aziją su meile", "Gabrielė Štaraitė", "publicistika", 1,
                "Vilnius", 181, false, "978-609-01-2020-0");


        System.out.print(kn1.getLeidimas() + "-asis knygos "  + "\"" + kn1.getPavadinimas() + "\"" + " leidimas yra ");
        kn1.pavadintiVirseli();
        kn1.setKietaisVirseliais(false);
        kn1.setLeidimas(1);
        System.out.print(kn1.getLeidimas() + "-asis knygos "  + "\"" + kn1.getPavadinimas() + "\"" + " leidimas buvo ");
        kn1.pavadintiVirseli();
        System.out.print(kn2.getAutorius() + " \"" + kn2.getPavadinimas() + "\"" + " " + kn2.getLeidimas() + "-ojo leidimo knygos išparduotos." );
        kn2.setLeidimas(kn2.getLeidimas() + 1);
        System.out.println(" Planuojamas " + kn2.getLeidimas() + "-asis knygos leidimas.");
        System.out.print(kn3.getLeidimas() + "-asis knygos "  + "\"" + kn3.getPavadinimas() + "\"" + " leidimas yra ");
        kn3.pavadintiVirseli();
        kn3.setKietaisVirseliais(true);
        kn3.setLeidimas(1);
        System.out.print(kn3.getLeidimas() + "-asis knygos "  + "\"" + kn3.getPavadinimas() + "\"" + " leidimas buvo ");
        kn3.pavadintiVirseli();
        System.out.print("KnygaImpl \"" + kn4.getPavadinimas() +  "\" buvo išleista 1967 m. Leidimo vieta - " + kn4.getIsleidimoVieta() + ".");
        kn4.setIsleidimoVieta("Klaipėda");
        kn4.setLeidimas(kn4.getLeidimas() + 1);
        System.out.println(" 2019 metais planuojamas " + kn4.getLeidimas() + "-asis leidimas. Planuojama išleidimo vieta - " + kn4.getIsleidimoVieta() + ".");
        System.out.print("A. Čekuolio knygos " + "\"" + kn5.getPavadinimas() + "\"" + " " + kn5.getLeidimas() + "-asis leidimas buvo " + kn5.getPuslapiuSkaicius() + " puslapių.");
        kn9.setPuslapiuSkaicius(kn5.getPuslapiuSkaicius()+10);
        kn5.setLeidimas(kn5.getLeidimas() + 1);
        System.out.println(" Naujausias, " + kn5.getLeidimas() + "-asis leidimas bus papildytas nuotraukomis ir bus " + kn9.getPuslapiuSkaicius() + " puslapių.");
        System.out.print("I. Yalomo " + kn6.getLeidimas() + "-asis knygos leidimas buvo  pavadintas " + "\"" + kn6.getPavadinimas() + "\".");
        kn6.setPavadinimas("Nyčės rauda");
        kn6.setLeidimas(kn6.getLeidimas() + 1);
        System.out.println(" Naujasis, " + kn6.getLeidimas() + "-ojo leidimo pavadinimas bus pakoreguotas. KnygaImpl vadinsis " + "\"" + kn6.getPavadinimas() + "\".");
        System.out.print(kn7.getAutorius() + " \"" + kn7.getPavadinimas() + "\"" + " " + kn7.getLeidimas() + "-ojo leidimo knygos išparduotos." );
        kn7.setLeidimas(kn7.getLeidimas() + 1);
        System.out.println(" Planuojamas " + kn7.getLeidimas() + "-asis knygos leidimas.");
        System.out.print("Pirmasis knygos " + "\"" + kn8.getPavadinimas() + "\"" + " leidimas buvo " + kn8.getPuslapiuSkaicius() + " puslapių.");
        kn8.setPuslapiuSkaicius(197);
        System.out.println(" Naujausias, " + kn8.getLeidimas() + "-asis" + " yra " + kn8.getPuslapiuSkaicius() + " puslapių.");
        System.out.print("Antrasis knygos " + "\"" + kn9.getPavadinimas() + "\"" + " leidimas buvo " + kn9.getPuslapiuSkaicius() + " puslapių.");
        kn9.setPuslapiuSkaicius(kn9.getPuslapiuSkaicius()+15);
        System.out.println(" Naujausias, " + kn9.getLeidimas() + "-asis" + " yra " + kn9.getPuslapiuSkaicius() + " puslapių.");
        System.out.print("KnygaImpl \"" + kn10.getPavadinimas() +  "\" buvo išleista 2017 m. Leidimo vieta - " + kn10.getIsleidimoVieta() + ".");
        kn10.setIsleidimoVieta("Ryga");
        kn10.setLeidimas(kn4.getLeidimas() + 1);
        System.out.println(" 2019 metais planuojamas " + kn10.getLeidimas() + "-asis leidimas. Planuojama išleidimo vieta - " + kn10.getIsleidimoVieta() + ".");

    }


    private String pavadinimas;
    private String autorius;
    private String zanras;
    private int leidimas;
    private String isleidimoVieta;
    private int puslapiuSkaicius;
    private boolean isKietaisVirseliais;
    private String isbnNumeris;


    public Knyga (String pavadinimas, String autorius, String zanras, int leidimas, String isleidimoVieta, int puslapiuSkaicius,
                  boolean isKietaisVirseliais, String isbnNumeris){
        this.pavadinimas = pavadinimas;
        this.autorius = autorius;
        this.zanras = zanras;
        this.leidimas = leidimas;
        this.isleidimoVieta = isleidimoVieta;
        this.puslapiuSkaicius = puslapiuSkaicius;
        this.isKietaisVirseliais = isKietaisVirseliais;
        this.isbnNumeris = isbnNumeris;
    }

    private void pavadintiVirseli(){
        if(isKietaisVirseliais == true){
            System.out.println("kietais viršeliais.");
        }else {
            System.out.println("minkštais viršeliais.");
        }
    }

    public String getPavadinimas() {
        return pavadinimas;
    }
    public void setPavadinimas(String pavadinimas) {
        this.pavadinimas = pavadinimas;
    }

    public String getAutorius(){
        return autorius;
    }
    public void setAutorius(String autorius){
        this.autorius = autorius;
    }

    public String getZanras(){
        return zanras;
    }
    public void setZanras(String zanras){
        this.zanras = zanras;
    }

    public int getLeidimas(){
        return leidimas;
    }
    public void setLeidimas(int leidimas) {
        this.leidimas = leidimas;
    }

    public String getIsleidimoVieta(){
        return isleidimoVieta;
    }
    public void setIsleidimoVieta(String isleidimoVieta){
        this.isleidimoVieta = isleidimoVieta;
    }

    public int getPuslapiuSkaicius(){
        return puslapiuSkaicius;
    }
    public void setPuslapiuSkaicius(int puslapiuSkaicius){
        this.puslapiuSkaicius = puslapiuSkaicius;
    }

    public boolean isKietaisVirseliais(){
        return isKietaisVirseliais;
    }

    public void setKietaisVirseliais(boolean isKietaisVirseliais){
        this.isKietaisVirseliais = isKietaisVirseliais;

    }
    public String getIsbnNumeris(){
        return isbnNumeris;
    }
    public void setIsbnNumeris(String isbnNumeris) {
      this.isbnNumeris = isbnNumeris;
    }

}


//1. Sukurti klasę KnygaImpl, panaudojant tinkamiausius kintamųjų tipus:
//2. Sukurti get'erius, set'erius ir konstruktorių.
//3. Sukurti 10 klasės objektų (skirtingų).
//4. Pakeisti kiekvieno objekto bent po 1 požymį.
//5. Atspausdinti objektus prieš ir po pakeitimų.