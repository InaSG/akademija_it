package Uzduotys.kitosUzduotys;

public class Masyvai {

    public static void main (String[] args) {
        double[] masyvas = {5, 9, -80, 6, -3, 8, -2, 4, 1, -4};
        spauzdintiMasyva(masyvas, masyvas.length);
        System.out.println("Masyvo lyginių skaičių vidurkis yra " +
                masyvoElemVidurkis (masyvas) +
                "(" + sudetiLyginius(masyvas, masyvas.length) + " / " + skaiciuotiLyginius(masyvas, masyvas.length) + ")");
        System.out.println("Didžiausia masyvo elementas yra "  + surastiMaxElementa(masyvas, masyvas.length));
        System.out.println("Mažiausia masyvo elementas yra " + surastiMinElementa(masyvas, masyvas.length));
        sukeistiMaxElementaSuMinElementu(masyvas);
        spauzdintiMasyva(masyvas, masyvas.length);
    }

    public static double masyvoElemVidurkis (double[] masyvas){
        double vidurkis = sudetiLyginius (masyvas, masyvas.length) / skaiciuotiLyginius (masyvas, masyvas.length);
        return vidurkis;
    }
    public static double sudetiLyginius(double[] masyvas, int n){
       double suma = 0;
        for (int i = 0; i<n; i++){
            if (i % 2 == 0){
            //if (masyvas[i] % 2 == 0) {
                //suma = suma + masyvas[i];
                suma = suma + masyvas[i];
            }
        }
        return suma;
    }
    public static double skaiciuotiLyginius (double[] masyvas, int n){
        int kiekis = 0;
        for (int i = 0; i < n; i++) {
            //if (masyvas[i] % 2 == 0) {
            if (i % 2 == 0){
                kiekis = kiekis + 1;
            }
        }
        return kiekis;
    }
    public static int surastiMaxElementa (double[] masyvas, int n){
        double maxReiksme = -999999999;
        int iMax = 0;
        for (int i = 0; i < n; i++){
            if (masyvas[i]> maxReiksme) {
                maxReiksme = masyvas[i];
                iMax = i;
            }
        }
        //return maxReiksme;
        return iMax;
    }
    public static int surastiMinElementa (double[] masyvas, int n){
        double minReiksme = 999999999;
        int iMin = 0;
        for (int i = 0; i < n; i++){
            if (masyvas[i] < minReiksme){
                minReiksme = masyvas[i];
                iMin = i;
            }
        }
        //return minReiksme;
        return iMin;

    }

    public static void sukeistiMaxElementaSuMinElementu (double[] masyvas){
        int j = surastiMinElementa(masyvas, masyvas.length);
        int k = surastiMaxElementa(masyvas, masyvas.length);
        double tarpineReiksme = masyvas[j];
        masyvas[j] = masyvas[k];
        masyvas[k] = tarpineReiksme;
    }

    public static void spauzdintiMasyva(double[] masyvas, int n){
        for (int i = 0; i < n; i++){
            System.out.print(masyvas[i] + "  ");
        }
    }
}
