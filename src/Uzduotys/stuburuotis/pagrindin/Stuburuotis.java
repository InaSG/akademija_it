
    package Uzduotys.stuburuotis.pagrindin;

    public interface Stuburuotis {
        public Integer getSlanksSkaicius();
        public void setSlanksSkaicius(Integer slanksSkaicius);
        public boolean isUodega();
        public Integer getKojuSkaicius();
        public void setKojuSkaicius (Integer kojuSkaicius);
        public String getPavadinimas();
        public void setPavadinimas(String odosSpalva);
    }
