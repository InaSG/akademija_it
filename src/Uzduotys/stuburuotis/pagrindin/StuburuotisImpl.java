package Uzduotys.stuburuotis.pagrindin;

public class StuburuotisImpl  implements Stuburuotis {
        private Integer slanksSkaicius;
        private boolean isUodega;
        private Integer kojuSkaicius;
        private String pavadinimas;

        public StuburuotisImpl(Integer slanksSkaicius, boolean isUodega, Integer kojuSkaicius, String pavadinimas){
            this.slanksSkaicius = slanksSkaicius;
            this.isUodega = isUodega;
            this.kojuSkaicius = kojuSkaicius;
            this.pavadinimas = pavadinimas;
        }

        public Integer getSlanksSkaicius(){
            return slanksSkaicius;
        }
        public void setSlanksSkaicius(Integer slanksSkaicius){
            this.slanksSkaicius = slanksSkaicius;
        }
        public boolean isUodega(){
            return isUodega;
        }
        public Integer getKojuSkaicius(){
            return kojuSkaicius;
        }
        public void setKojuSkaicius (Integer kojuSkaicius){
            this.kojuSkaicius = kojuSkaicius;
        }
        public String getPavadinimas(){
            return pavadinimas;
        }
        public void setPavadinimas(String odosSpalva){
            this.pavadinimas = pavadinimas;
        }
    }








