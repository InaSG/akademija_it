package Uzduotys.stuburuotis.isvest;

import Uzduotys.stuburuotis.pagrindin.StuburuotisImpl;

    public class StambusKanopinisImpl extends StuburuotisImpl  implements StambusKanopinis{
        private Integer raguSkaicius;
        private String kanoposForma;
        private Double kailioStoris;
        private boolean isDedaKiausinius;

        public StambusKanopinisImpl(Integer slanksSkaicius, boolean isUodega, Integer kojuSkaicius, String pavadinimas, Integer raguSkaicius, String kanoposForma, Double kailioStoris, boolean isDedaKiausinius){
            super(slanksSkaicius, isUodega, kojuSkaicius, pavadinimas);
            this.raguSkaicius = raguSkaicius;
            this.kanoposForma = kanoposForma;
            this.kailioStoris = kailioStoris;
            this.isDedaKiausinius = isDedaKiausinius;
        }

        public Integer getRaguSkaicius(){
            return raguSkaicius;
        }
        public void setRaguSkaicius(Integer raguSkaicius){
            this.raguSkaicius = raguSkaicius;
        }
        public String getKanoposForma(){
            return kanoposForma;
        }
        public void setKanoposForma(String kanoposForma){
            this.kanoposForma = kanoposForma;
        }
        public Double getKailioStoris(){
            return kailioStoris;
        }
        public void setKailioStoris(Double kailioStoris){
            this.kailioStoris = kailioStoris;
        }
        public boolean getisDedaKiausinius(){
            return isDedaKiausinius;
        }

    }



