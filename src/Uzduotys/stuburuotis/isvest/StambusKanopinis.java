package Uzduotys.stuburuotis.isvest;

    public interface StambusKanopinis {
        public Integer getRaguSkaicius();
        public void setRaguSkaicius(Integer raguSkaicius);
        public String getKanoposForma();
        public void setKanoposForma(String kanoposForma);
        public Double getKailioStoris();
        public void setKailioStoris(Double kailioStoris);
        public boolean getisDedaKiausinius();
    }

