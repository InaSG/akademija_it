package Uzduotys.stuburuotis;

import java.util.*;
import java.util.function.Predicate;


import Uzduotys.stuburuotis.isvest.StambusKanopinis;
import Uzduotys.stuburuotis.isvest.StambusKanopinisImpl;
import Uzduotys.stuburuotis.pagrindin.Stuburuotis;
import Uzduotys.stuburuotis.pagrindin.StuburuotisImpl;

public class MainStuburuotis {
    public static void main(String[] args) {
        ArrayList<StambusKanopinisImpl> kanopiniai = new ArrayList(10);

        kanopiniai.add(new StambusKanopinisImpl(16, true, 4, "raganosis", 1, "labai didele", 0.1, false));
        kanopiniai.add(new StambusKanopinisImpl(24, true, 4, "briedis", 2, "pailga", 2.2, false));
        kanopiniai.add(new StambusKanopinisImpl(20, true, 4, "sernas", 0, "labai maza", 3.5, false));
        kanopiniai.add(new StambusKanopinisImpl(28, true, 4, "dramblys", 0, "penkiapirste", 0.2, false));
        kanopiniai.add(new StambusKanopinisImpl(22, true, 4, "karve", 2, "didele", 1.0, false));
        kanopiniai.add(new StambusKanopinisImpl(26, true, 4, "stirna", 2, "nedidele", 3.2, false));
        kanopiniai.add(new StambusKanopinisImpl(56, false, 3, "mistinis1", 4, "kvadratas", 43.3, true));
        kanopiniai.add(new StambusKanopinisImpl(8, false, 2, "mistinis2", 7, "plokscia", 17.0, true));
        kanopiniai.add(new StambusKanopinisImpl(6, false, 3, "mistinis3", 44, "trikampe", 43.3, true));
        kanopiniai.add(new StambusKanopinisImpl(28, false, 2, "mistinis4", 5, "apskrita", 17.0, true));

        printArrayList(kanopiniai);
        System.out.println();

        sortByKanoposForma(kanopiniai);
        printArrayList(kanopiniai);
        System.out.println();

        sortByRaguSkaiciu(kanopiniai);
        printArrayList(kanopiniai);
        System.out.println();

        removeKanopinis(kanopiniai);
        printArrayList(kanopiniai);
    }

    static void printArrayList(ArrayList<StambusKanopinisImpl> kanopiniai) {
        for (int i = 0; i < kanopiniai.size(); i++) {
            printKanopinis(kanopiniai.get(i));
        }
    }


//    static void printArrayList2(ArrayList<StambusKanopinisImpl>kanopiniai) {
//        Iterator<StambusKanopinisImpl> StambusKanopinisIterator = kanopiniai.iterator();
//        while (StambusKanopinisIterator.hasNext()) {
//            printKanopinis(StambusKanopinisIterator.next());
//        }
//    }

    static void printKanopinis(StambusKanopinisImpl StambusKanopinis) {
        System.out.print(StambusKanopinis.getSlanksSkaicius());
        System.out.print(" ,");
        System.out.print(StambusKanopinis.isUodega());
        System.out.print(" ,");
        System.out.print(StambusKanopinis.getKojuSkaicius());
        System.out.print(" ,");
        System.out.print(StambusKanopinis.getPavadinimas());
        System.out.print(" ,");
        System.out.print(StambusKanopinis.getRaguSkaicius());
        System.out.print(" ,");
        System.out.print(StambusKanopinis.getKanoposForma());
        System.out.print(" ,");
        System.out.print(StambusKanopinis.getKojuSkaicius());
        System.out.print(" ,");
        System.out.print(StambusKanopinis.getKailioStoris());
        System.out.print(" ,");
        System.out.println(StambusKanopinis.getisDedaKiausinius());
    }

    static void sortByKanoposForma(ArrayList<StambusKanopinisImpl> kanopinis){
        Collections.sort(kanopinis, new Comparator<StambusKanopinisImpl>() {
            @Override
            public int compare(StambusKanopinisImpl o1, StambusKanopinisImpl o2) {
                return o2.getKanoposForma().compareTo(o1.getKanoposForma());
            }
        });
    }

    static void sortByRaguSkaiciu(ArrayList<StambusKanopinisImpl> kanopinis){
        Collections.sort(kanopinis, new Comparator<StambusKanopinisImpl>() {
            @Override
            public int compare(StambusKanopinisImpl o1, StambusKanopinisImpl o2) {
                return o1.getRaguSkaicius().compareTo(o2.getRaguSkaicius());
            }
        });
    }

    static void removeKanopinis(ArrayList<StambusKanopinisImpl> kanopinis){
        kanopinis.removeIf(new Predicate<StambusKanopinisImpl>() {
            @Override
            public boolean test(StambusKanopinisImpl stambusKanopinis) {
                return stambusKanopinis.getKojuSkaicius() < 4;
            }
        });
    }

}


