package Uzduotys.zmogus.isvestine;

import Uzduotys.zmogus.pagrindine.ZmogusImpl;

public class StatybininkasImpl extends ZmogusImpl implements Statybininkas{
    private int stazas;
    private String issilavinimas;
    private String specializacija;
    private Double atlyginimas;

    public StatybininkasImpl(boolean isMoteris, String asmKodas, String vardas, String pavarde, int amzius, double svoris, double ugis, String plaukuSpalva, int stazas, String issilavinimas, String specializacija, Double atlyginimas) {
        super(isMoteris, asmKodas, vardas, pavarde, amzius, svoris, ugis, plaukuSpalva);
        this.stazas = stazas;
        this.issilavinimas = issilavinimas;
        this.specializacija = specializacija;
        this.atlyginimas = atlyginimas;
    }

    @Override
    public int getStazas() {
        return stazas;
    }

    @Override
    public void setStazas(int stazas) {
        this.stazas = stazas;
    }

    @Override
    public String getIssilavinimas() {
        return issilavinimas;
    }

    @Override
    public void setIssilavinimas(String issilavinimas) {
        this.issilavinimas = issilavinimas;
    }

    @Override
    public String getSpecializacija() {
        return specializacija;
    }

    @Override
    public void setSpecializacija(String specializacija) {
        this.specializacija = specializacija;
    }

    public Double getAtlyginimas() {
        return atlyginimas;
    }

    @Override
    public void setAtlyginimas(Double atlyginimas) {
        this.atlyginimas = atlyginimas;
    }
}

