package Uzduotys.zmogus.isvestine;

public interface Statybininkas {
    public int getStazas();
    public void setStazas(int stazas);
    public String getIssilavinimas();
    public void setIssilavinimas(String issilavinimas);
    public String getSpecializacija();
    public void setSpecializacija(String specializacija);
    public Double getAtlyginimas();
    public void setAtlyginimas(Double atlyginimas);
}
