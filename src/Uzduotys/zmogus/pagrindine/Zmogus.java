package Uzduotys.zmogus.pagrindine;

public interface Zmogus {
    public boolean isMoteris();
    public String getAsmKodas();
    public void setAsmKodas(String asmKodas);
    public int getAmzius();
    public void setAmzius(int amzius);
    public String getVardas();
    public void setVardas(String vardas);
    public String getPavarde();
    public void setPavarde(String pavarde);
    public double getSvoris();
    public void setSvoris(double svoris);
    public double getUgis();
    public void setUgis(double ugis);
    public String getPlaukuSpalva();
    public void setPlaukuSpalva(String plaukuSpalva);
}
