package Uzduotys.zmogus.pagrindine;

public class ZmogusImpl implements Zmogus{
    private boolean isMoteris;
    private String asmKodas;
    private String vardas;
    private String pavarde;
    private int amzius;
    private double svoris;
    private double ugis;
    private String plaukuSpalva;


    public ZmogusImpl(boolean isMoteris, String asmKodas, String vardas, String pavarde, int amzius, double svoris, double ugis, String plaukuSpalva) {
        this.isMoteris = isMoteris;
        this.asmKodas = asmKodas;
        this.vardas = vardas;
        this.pavarde = pavarde;
        this.amzius = amzius;
        this.svoris = svoris;
        this.ugis = ugis;
        this.plaukuSpalva = plaukuSpalva;
    }

    public boolean isMoteris() {
        return isMoteris;
    }

    public String getAsmKodas() {
        return asmKodas;
    }

    public void setAsmKodas(String asmKodas) {
        this.asmKodas = asmKodas;
    }

    public int getAmzius() {
        return amzius;
    }

    public void setAmzius(int amzius) {
        this.amzius += amzius;
    }

    public String getVardas() {
        return vardas;
    }

    public void setVardas(String vardas) {
        this.vardas = vardas;
    }

    public String getPavarde() {

        return pavarde;
    }

    public void setPavarde(String pavarde) {

        this.pavarde = pavarde;
    }

    public double getSvoris() {

        return svoris;
    }

    public void setSvoris(double svoris) {

        this.svoris += svoris; // this.svoris=this.svoris + svoris
    }

    public double getUgis() {
        return ugis;
    }

    public void setUgis(double ugis) {
        this.ugis += ugis;
    }

    public String getPlaukuSpalva() {
        return plaukuSpalva;
    }

    public void setPlaukuSpalva(String plaukuSpalva) {
        this.plaukuSpalva = plaukuSpalva;
    }

}

//1. Sukurti klasę ZmogusImpl:
//lytis[nekeičiama];
//asmens kodas;
//vardas;
//pavardė;
//amžius;
//svoris;
//ūgis;
//plaukų spalva;
//2. Sukurti metodus pasiimti ir pakeisti kintamųjų
//reikšmes ir konstruktorių (su parametrais).
//3. Sukurti 6 skirtingus objektus (žmones).
//4. Pabandyti pakeisti objektų kintamuosius per metodus ir
//atspausdinti vartotojui duomenis apie objektą.
