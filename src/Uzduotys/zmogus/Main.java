package Uzduotys.zmogus;

import java.util.*;
import java.util.ArrayList;
import java.util.function.Predicate;

import Uzduotys.zmogus.pagrindine.ZmogusImpl;
import Uzduotys.zmogus.isvestine.StatybininkasImpl;

public class Main {
    public static void main(String[] args) {
        ArrayList<StatybininkasImpl> statybininkai = new ArrayList(10);

        statybininkai.add(new StatybininkasImpl(false, "456788335", "Romas", "Romanovas", 20, 55, 167, "juodi", 2, "profesinis", "gruščikas", 600D));
        statybininkai.add(new StatybininkasImpl(false, "3456788", "Nerka", "Nerkavičius", 56, 98, 187, "juodi", 1, "aukštasis", "tinkuotojas", 2000D));
        statybininkai.add(new StatybininkasImpl(false, "5678345698", "Tomas", "Tomkus", 18, 123, 189, "šviesūs", 1, "vidurinis", "dažytojas", 650D));
        statybininkai.add(new StatybininkasImpl(false, "34567893434", "Vidas", "Vidinis", 67, 88, 183, "žilas", 37, "pradinis", "mūrininkas", 3000D));
        statybininkai.add(new StatybininkasImpl(false, "33456456567", "Jonas", "Jonynas", 46, 67, 176, "rudi", 25, "aukštasis", "skardininkas", 1500D));
        statybininkai.add(new StatybininkasImpl(true, "4987653435", "Vandutė", "Antanienė", 53, 67, 170, "rudi", 15, "aukštasis", "dažytoja", 1700D));
        statybininkai.add(new StatybininkasImpl(true, "4987654345", "Aldona", "Antanytė", 33, 84, 173, "šviesūs", 1, "vidurinis", "dažytoja", 650D));
        statybininkai.add(new StatybininkasImpl(false, "345674567887", "Petras", "Petraitis", 39, 68, 175, "juodi", 4, "aukštasis", "mūrininkas", 1000D));
        statybininkai.add(new StatybininkasImpl(false, "312345678", "Kestutis", "Kestutaitis", 40, 125, 189, "šviesūs", 12, "aukštasis", "apdailininkas", 2000D));
        statybininkai.add(new StatybininkasImpl(false, "59857698787", "Darius", "Darulis", 25, 77, 180, "rudi", 8, "pradinis", "tinkuotojas", 1500D));

        print1(statybininkai);
        print2(statybininkai);
        print3(statybininkai);

        sortByVardas(statybininkai);
        print1(statybininkai);

        sortByAtlyginima(statybininkai);
        print1(statybininkai);

        removeStatybininkas(statybininkai);
        print1(statybininkai);

    }

    static void print1(ArrayList<StatybininkasImpl> statybininkai){
        for(int i = 0; i < statybininkai.size(); i++){
            printStatybininkas(statybininkai.get(i));
        }
    }

    static void print2(ArrayList<StatybininkasImpl> statybininkai){
        Iterator<StatybininkasImpl> statybininkasIterator = statybininkai.iterator();
        while (statybininkasIterator.hasNext()){
            printStatybininkas(statybininkasIterator.next());
        }
    }

    static void print3(ArrayList<StatybininkasImpl> statybininkai){
        for (StatybininkasImpl statybininkas : statybininkai) {
            printStatybininkas(statybininkas);
        }
    }

    static void sortByVardas(ArrayList<StatybininkasImpl> statybininkai){
        Collections.sort(statybininkai, new Comparator<StatybininkasImpl>() {
            @Override
            public int compare(StatybininkasImpl o1, StatybininkasImpl o2) {
                return o1.getVardas().compareTo(o2.getVardas());
            }
        });
    }

    static void sortByAtlyginima(ArrayList<StatybininkasImpl> statybininkai){
        Collections.sort(statybininkai, new Comparator<StatybininkasImpl>() {
            @Override
            public int compare(StatybininkasImpl o1, StatybininkasImpl o2) {
                return o1.getAtlyginimas().compareTo(o2.getAtlyginimas());
            }
        });
    }

    static void printStatybininkas(StatybininkasImpl statybininkas){
        System.out.println(statybininkas.getVardas());
    }

    static void removeStatybininkas(ArrayList<StatybininkasImpl> statybininkai){
        statybininkai.removeIf(new Predicate<StatybininkasImpl>() {
            @Override
            public boolean test(StatybininkasImpl statybininkas) {
                return statybininkas.getStazas() < 4;
            }
        });
    }
}

