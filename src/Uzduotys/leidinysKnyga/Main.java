package Uzduotys.leidinysKnyga;
import Uzduotys.leidinysKnyga.knyga.KnygaImpl;
import Uzduotys.leidinysKnyga.leidinys.LeidinysImpl;

public class Main {
    public static void main(String[] args) {
        LeidinysImpl leidinys1 = new LeidinysImpl("zurnalas", true, 47);
        LeidinysImpl leidinys2 = new LeidinysImpl("laikrastis", true, 24);
        LeidinysImpl leidinys3 = new LeidinysImpl("laikrastis", true, 38);
        LeidinysImpl leidinys4 = new LeidinysImpl("kalendorius", false, 368);
        LeidinysImpl leidinys5 = new LeidinysImpl("monografija", false, 215);
        KnygaImpl knyga1 = new KnygaImpl("knyga", false, 375, "Kai Nyčė verkė", true, "romanas");
        KnygaImpl knyga2 = new KnygaImpl("knyga", false, 139, "Lietuviškoji trilogija", true, "poezija");
        KnygaImpl knyga3 = new KnygaImpl("knyga", false, 194, "Trise valtimi", false, "apysaka");
        KnygaImpl knyga4 = new KnygaImpl("knyga", false, 168, "Šachmatų vadovėlis", true, "vadovelis");
        KnygaImpl knyga5 = new KnygaImpl("knyga", false, 181, "Į Aziją su meile", false, "publicistika");

        spausdinti(knyga1, knyga2, knyga3, knyga4, knyga5);
    }

    public static void spausdinti(KnygaImpl... args) {
        for (int i = 0; i < args.length; i++) {
            System.out.print(args[i].getTipas() + ", ");
            System.out.print("\"" + args[i].getPavadinimas() + "\", ");
            System.out.print(args[i].getPuslapiuSkaicius() + " psl, ");
            if (args[i].isKietaisVirseliais() == true) {
                System.out.print("kietais viršeliais" + ".");
            } else System.out.print("minkštais viršeliais" + ".");

            System.out.println();
        }
    }
}



