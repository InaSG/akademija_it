package Uzduotys.leidinysKnyga.knyga;

public interface Knyga {
    public String getPavadinimas();

    public void setPavadinimas(String pavadinimas);

    public boolean isKietaisVirseliais();

    public String getTipas();

    public void setTipas(String tipas);
}
