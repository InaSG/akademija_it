package Uzduotys.leidinysKnyga.knyga;

import Uzduotys.leidinysKnyga.leidinys.LeidinysImpl;

public class KnygaImpl extends LeidinysImpl implements Knyga {
    private String pavadinimas;
    private boolean isKietaisVirseliais;
    private String tipas;

    public KnygaImpl(String rusis, boolean isPeriodinis, int puslapiuSkaicius, String pavadinimas, boolean isKietaisVirseliais, String tipas){
        super(rusis, isPeriodinis, puslapiuSkaicius);
        this.pavadinimas = pavadinimas;
        this.isKietaisVirseliais = isKietaisVirseliais;
        this.tipas = tipas;
    }

    @Override
    public String getPavadinimas(){
        return pavadinimas;
    }
    @Override
    public void setPavadinimas(String pavadinimas){
        this.pavadinimas = pavadinimas;
    }
    @Override
    public boolean isKietaisVirseliais(){
        return isKietaisVirseliais;
    }
    @Override
    public String getTipas (){
        return tipas;
    }
    @Override
    public void setTipas (String tipas){
        this.tipas = tipas;
    }
}

