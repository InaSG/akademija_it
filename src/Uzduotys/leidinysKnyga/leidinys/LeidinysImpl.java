package Uzduotys.leidinysKnyga.leidinys;

public class LeidinysImpl implements Leidinys {
    private String rusis;
    private boolean isPeriodinis;
    private int puslapiuSkaicius;

public LeidinysImpl (String rusis, boolean isPeriodinis, int PuslapiuSkaicius){
this.rusis = rusis;
this.isPeriodinis = isPeriodinis;
this.puslapiuSkaicius = PuslapiuSkaicius;
}

public String getRusis(){
    return rusis;
}
public void setRusis(String rusis){
    this.rusis = rusis;
}
public boolean getIsPeriodinis(){
    return isPeriodinis;
}
public int getPuslapiuSkaicius (){
    return puslapiuSkaicius;
}
public void setPuslapiuSkaicius(int puslapiuSkaicius){
    this.puslapiuSkaicius = puslapiuSkaicius;
}
}