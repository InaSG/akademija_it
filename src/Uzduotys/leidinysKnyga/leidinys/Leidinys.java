package Uzduotys.leidinysKnyga.leidinys;

public interface Leidinys {
    public String getRusis();

    public void setRusis(String rusis);

    public boolean getIsPeriodinis();

    public int getPuslapiuSkaicius();

    public void setPuslapiuSkaicius(int puslapiuSkaicius);
}
